//For problem statement of this code checkout this link: 
//https://leetcode.com/problems/word-search/
public class WordSearch {
      public static void main(String args[]){
            char[][] board = {{'A','B','C','E'},
                              {'S','F','E','S'},
                              {'A','D','E','E'}};

		System.out.println("Is Word Exist: "+Helper.exist(board, "ABCEFSADEESE"));
      }
}
class Helper {
    
    private static char[][] dots;
    private static char[][] board; 
    public static boolean exist(char[][] brd, String word) {
        board = brd;
        if(board.length * board[0].length < word.length()){
            return false;
        }
        for(int i=0;i<board.length;i++){
            for(int j=0;j<board[0].length;j++){
                dots = new char[board.length][board[0].length];
                if(Help(i, j, word, 0)){return true;}
            }
        }
        return false;
    }
    private static boolean Help(int i, int j, String word, int n){
        if(n == word.length()){
            return true;
        }
        if(board[i][j] != word.charAt(n)){
            return false;
        }
        dots[i][j] = '.';
        n++;
        if(i != 0 && dots[i-1][j] != '.'){
            i--;
            if(Help(i, j, word,n)){
                return true;
            }
            i++;
        }
        if(j != board[0].length-1 && dots[i][j+1] != '.' ){
            j++;
            if(Help(i, j, word, n)){
                return true;
            }
            j--;
        }
        if(i != board.length-1 && dots[i+1][j] != '.'){
            i++;
            if(Help(i, j, word, n)){
                return true;
            }
            i--;
        }
        if(j != 0 && dots[i][j-1] != '.'){
            j--;
            if(Help(i, j, word, n)){
                return true;
            }
            j++;
        }
        if(n == word.length() ) {
            return true;
        }
        dots[i][j] = ' ';
        return false;
    }
}
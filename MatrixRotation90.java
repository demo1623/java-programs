public class MatrixRotation90 {
      public static void main(String args[]) {
            int[][] matrix = { { 1, 2, 3 }, { 4, 5, 6 }, { 7, 8, 9 } }; // Always enter same column length and row length inputs
            System.out.println("Before: ");
            for (int i = 0; i < matrix.length; i++) {
                  for (int j = 0; j < matrix.length; j++) {
                        System.out.print(matrix[i][j] + " ");
                  }
                  System.out.println();
            }
            Helper.Rotate90Degree(matrix);
      }
}

class Helper {
      public static void Rotate90Degree(int[][] matrix) {
            int n = matrix.length;
            int[][] ans = new int[n][n];
            System.out.println("After: ");
            for (int i = 0; i < n; i++) {
                  for (int j = n - 1; j >= 0; j--) {
                        ans[i][(n-1)-j] = matrix[j][i];
                  }
            }
            for (int i = 0; i < n; i++) {
                  for (int j = 0; j < n; j++) {
                        System.out.print(ans[i][j] + " ");
                  }
                  System.out.println();
            }
      }
}
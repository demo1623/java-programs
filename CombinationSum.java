// Note: Enter Distinct candidates 
// Finds the target Sum from candidates (All possibilities)

import java.util.*;
import java.util.stream.*;
public class CombinationSum {
      public static void main(String[] args){
            int[] candidates = {3,7,8,2,6,9};
            int Target = 15;
            for(List<Integer> temp : (Helper.combinationSum(candidates, Target)))
                  System.out.println("Sum of: "+temp.toString() + " = "+ Target);
      }
}
class Helper {
      static int[][] matrix ;
      static Set<String> set ;
      static List<List<Integer>> anss;
      public static List<List<Integer>> combinationSum(int[] candidates, int target) {
          int min = 41;
          for(int i: candidates){
              if(i<min){
                  min = i;
              }
          }
          int sum = 0;
          int width=1;
          while(true){
              sum+=min;
              if(sum>=target){
                  break;
              }
              width++;
          }
          matrix = new int[width][candidates.length];
          set = new HashSet<String>();
          for(int i=0;i<width;i++){
              matrix[i] = candidates;
          }
          anss = new ArrayList<List<Integer>>();
          Help(0, new ArrayList<Integer>(), target);
          return anss;
      }
      public static void Help(int n, List<Integer> ans, int target){
          int sum = 0;
          for(int i=0;i<ans.size();i++){
              sum += ans.get(i);
              if(sum > target){
                  // System.out.println("Sum: "+sum);
                  return;
              }
          }
          if(sum == target){
              List<Integer> clone = new ArrayList<Integer>(ans);
              clone = clone.stream().sorted().collect(Collectors.toList()); 
            //   Collections.sort(clone);
              if(!set.contains(clone.toString())){
                  set.add(clone.toString());
                  anss.add(clone);
                  // System.out.println("weg: "+clone.toString());
                  return;
              }
              // System.out.println(clone.toString());
              // System.out.println("Je");
              return;
          }
          if(n == matrix.length){
              // System.out.println("wehgwg: "+ans.toString());
              return;
          }
          for(int i=0;i<matrix[0].length;i++){
              ans.add(matrix[n][i]);
              Help(n+1, ans, target);
              ans.remove(ans.size()-1);
          }
      }
  }
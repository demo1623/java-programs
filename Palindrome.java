class Helper{
      public static boolean Palindrome(String str, int Front, int Rear){
            if(Rear <= Front){
                return true;
            }
            if(str.charAt(Front) != str.charAt(Rear)){
                return false;
            }
            return Palindrome(str, Front+1, Rear-1);
      }
}
public class Palindrome
{
	public static void main(String[] args) {
		System.out.println(Helper.Palindrome("aba", 0, "aba".length()-1));
		System.out.println(Helper.Palindrome("Neel", 0, "Neel".length()-1));
	}
	
}

//For problem statement of this code checkout this link: 
//https://leetcode.com/problems/guess-number-higher-or-lower/
class Helper{
      public static int Gnum;
      static int guess(int num){
            if(num<Gnum){
                  return 1;
            }
            else if(num>Gnum){
                  return -1;
            }
            return 0;
      }
      public static int guessed(int s, int e){
            int mid = s+((e-s)/2);
            if(guess(mid) == 0){
                return mid;
            }
            else if(guess(e) == 0){
                return e;
            }
            else if(guess(mid) == 1){
                return guessed(mid, e);
            }
            return guessed(s, mid);
        }
}
public class GuessNumberHigherLower {
      public static void main(String args[]) {
            int n = 10;
            Helper.Gnum = 6;
            System.out.println(Helper.guessed(1, n));
      }
}

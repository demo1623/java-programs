//For problem statement of this code checkout this link: 
//https://leetcode.com/problems/zigzag-conversion/
class Helper{
      public static String convert(String s, int numRows) {
            if (s.length() == 1 || numRows == 1) {
                  return s;
            }
            char ans[][] = new char[numRows][s.length() / 2 + 1];
            ans = ZigZag(s, ans, numRows, 0, 0, 0);
            s = "";
            for (int i = 0; i < ans.length; i++) {
                  for (int j = 0; j < ans[i].length; j++) {
                        System.out.print(ans[i][j] + "   " );
                        if (((int) ans[i][j] >= 97 && (int) ans[i][j] <= 122)
                                    || ((int) ans[i][j] >= 65 && (int) ans[i][j] <= 90) || (int) ans[i][j] == 46
                                    || (int) ans[i][j] == 44) {
                              s += ans[i][j];
                        }
                  }
                  System.out.println();
            }
            return s;
      }

      static boolean mode = false;

      public static char[][] ZigZag(String s, char ans[][], int numRows, int r, int c, int n) {
            if (n == s.length()) {
                  return ans;
            }
            if (mode && r == 0) {
                  ans[++r][c] = s.charAt(n);
                  n++;
                  mode = false;
                  return ZigZag(s, ans, numRows, ++r, c, n);
            }
            if (r < numRows && mode == false) {
                  ans[r][c] = s.charAt(n);
                  n++;
                  return ZigZag(s, ans, numRows, ++r, c, n);
            }
            if (mode == false) {
                  mode = true;
                  r--;
            }
            r--;
            c++;
            ans[r][c] = s.charAt(n);
            // System.out.print(" R: "+ r+ " C: "+c +" n: "+n+"\n");
            n++;
            return ZigZag(s, ans, numRows, r, c, n);
      }
}
public class ZigZagConversion {
      public static void main(String args[]){
            String s = "NEELCHHATBAR";
            int Rows = 4;
            System.out.println(Helper.convert(s, Rows));
      }
}
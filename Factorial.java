import java.util.Scanner;
class Helper {
	static int fact(int n) {
		return (n == 0)?1:n*fact(n-1);
	}
}

class Factorial {
	public static void main(String args[]) {
		Scanner sc = new Scanner(System.in);
		System.out.println("Enter a Number to calculate factorial : ");
		System.out.println("Factorial is: " + Helper.fact(sc.nextInt()));
	}
}
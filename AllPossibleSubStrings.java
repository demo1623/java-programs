
class Helper{
	public static void Permutations(String str, int i, String cur){
	    if(i == str.length()){
	        System.out.println(cur);
	        return ;
	    } 
	    Permutations(str, i+1, cur+str.charAt(i));
	    Permutations(str, i+1, cur);
	}
}
public class AllPossibleSubStrings
{
	public static void main(String[] args) {
		Helper.Permutations("abc", 0, "");
	}
}
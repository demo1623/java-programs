//For problem statement of this code checkout this link: 
//https://leetcode.com/problems/remove-element/
class Helper{
      public static int removeElement(int[] nums, int val) {
            int k = 0;
            for (int i = 0; i < nums.length; i++) {
                  if (nums[i] == val) {
                        k++;
                  }
            }
            int temp[] = new int[nums.length - k];
            int index = 0;
            for (int i = 0; i < nums.length; i++) {
                  if (nums[i] != val) {
                        temp[index] = nums[i];
                        index++;
                  }
            }
            for (int i = 0; i < temp.length; i++) {
                  nums[i] = temp[i];
            }
            return temp.length;
      }
}
public class RemoveElement {
      public static void main(String args[]){
            int arr[] = {3,2,2,3};
            int val = 3;
            val = Helper.removeElement(arr, val);
            System.out.print("[ ");
            for(int i=0;i<val;i++){
                  System.out.print(arr[i]+" ");  
            } 
            System.out.print("]");
      }
}

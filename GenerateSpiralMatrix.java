//Note: StartsFrom must be Greater than 0
// It generates nXn spiral matrix from Given StartsFrom value
public class GenerateSpiralMatrix {
      public static void main(String[] args){
            for(int[] i:Helper.generateMatrix(5, 10)){
                  for(int j: i){
                        System.out.print(j+ " ");
                  }
                  System.out.println();
            }
      }
}
class Helper {
      static int[][] board ;
      public static int[][] generateMatrix(int n, int startsFrom) {
          board = new int[n][n];
          Help(0, 0, startsFrom);
          return board;
      }
      public static void Help( int i, int j, int n){
          board[i][j] = n;
          if((i != 0 && board[i-1][j] == 0) && (j != board[0].length-1 && board[i][j+1] == 0)){
              Help( i-1, j, n+1);
          }
          else if(j != board[0].length-1 && board[i][j+1] == 0){
              Help( i, j+1, n+1);
          }
          else if(i != board.length-1 && board[i+1][j] == 0){
              Help( i+1, j, n+1);
          } 
          else if(j != 0 && board[i][j-1] == 0){
              Help( i, j-1, n+1);
          }
          else if(i != 0 && board[i-1][j] == 0){
              Help( i-1, j, n+1);
          }
      }
  }
  
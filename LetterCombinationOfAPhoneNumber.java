// Don't Enter More than 10 Digits as input (it'll hang your computer)
//For problem statement of this code checkout this link:   (medium level)
// https://leetcode.com/problems/letter-combinations-of-a-phone-number/
import java.util.*;

public class LetterCombinationOfAPhoneNumber {
      public static void main(String args[]) {
            System.out.println("All Possible Combinations Are: \n"+(Helper.letterCombinations("27")).toString());
      }
}

class Helper {
      static String[] vals = { "", "", "abc", "def", "ghi", "jkl", "mno", "pqrs", "tuv", "wxyz" };
      static List<String> ans;

      public static List<String> letterCombinations(String digits) {
            ans = new ArrayList<String>();
            if (digits.equals("")) {
                  return ans;
            }
            Help(digits, 0, "");
            return ans;
      }

      public static void Help(String digs, int n, String up) {
            if (n == digs.length()) {
                  ans.add(up);
                  return;
            }
            int index = Integer.parseInt(String.valueOf(digs.charAt(n)));
            for (int i = 0; i < vals[index].length(); i++) {
                  up += vals[index].charAt(i);
                  Help(digs, n + 1, up);
                  up = up.substring(0, up.length() - 1);
            }
      }
}
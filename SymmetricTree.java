import java.util.ArrayList;
// -100 <= Node.val <= 100
// 1 <= Nodes.length <= 1000
//Sample Binary Symmetric Tree 👇
//        1
//     /     \
//    2       2
//  /  \    /  \
// 3    4  4    3
public class SymmetricTree {
      public static void main(String args[]){
            TreeNode Tree = new TreeNode();
            Tree.val = 1;
            Tree.left = new TreeNode(2);
            Tree.right = new TreeNode(2);
            (Tree.left).left = new TreeNode(3);
            (Tree.left).right = new TreeNode(4);
            (Tree.right).left = new TreeNode(4);
            (Tree.right).right = new TreeNode(3);
            System.out.println("Is Tree Symmetric: "+Helper.isSymmetric(Tree));
      }
}

// Definition for a binary tree node.
class TreeNode {
      int val;
      TreeNode left;
      TreeNode right;

      TreeNode() {
      }

      TreeNode(int val) {
            this.val = val;
      }

      TreeNode(int val, TreeNode left, TreeNode right) {
            this.val = val;
            this.left = left;
            this.right = right;
      }
}

class Helper {
      static ArrayList<Integer> PreOrder = new ArrayList<Integer>();
      static ArrayList<Integer> MPreOrder = new ArrayList<Integer>();

      public static boolean isSymmetric(TreeNode root) {
            PreOrder = new ArrayList<Integer>();
            MPreOrder = new ArrayList<Integer>();
            CalculatePreOrder(root);
            CalculateMirrorPreOrder(root);
            while(PreOrder.remove(Integer.valueOf(Integer.MIN_VALUE)) || MPreOrder.remove(Integer.valueOf(Integer.MIN_VALUE))){}
            System.out.println("PreOrder:          "+PreOrder);
            System.out.println("Mirrored PreOrder: "+MPreOrder);

            if (MPreOrder.equals(PreOrder)) {
                  return true;
            }
            return false;
      }

      public static void CalculatePreOrder(TreeNode root) {
            if (root == null) {
                  PreOrder.add(Integer.MIN_VALUE);
                  return;
            }
            PreOrder.add(root.val);
            CalculatePreOrder(root.left);
            CalculatePreOrder(root.right);
      }

      public static void CalculateMirrorPreOrder(TreeNode root) {
            if (root == null) {
                  MPreOrder.add(Integer.MIN_VALUE);
                  return;
            }
            MPreOrder.add(root.val);
            CalculateMirrorPreOrder(root.right);
            CalculateMirrorPreOrder(root.left);
      }
}
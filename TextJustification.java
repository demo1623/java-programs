//For problem statement of this code checkout this link: 
//https://leetcode.com/problems/text-justification/
//Note: Do not enter Maxwidth less than Any word's length 
import java.util.*;
public class TextJustification {
      public static void main(String[] args ){
            String[] strs = {"I'm", "Neel", "a","18","years","old","Mern", "Stack","Developer"};
            for(String s: Helper.fullJustify(strs, 16)){         //maxWidth is the length of perticular string made from words, all the words will fit into this width
                  System.out.println(s);
            }
      }
}

class Helper {
      public static List<String> fullJustify(String[] words, int maxWidth) {
          List<String> ans = new ArrayList<String>();
          List<List<Integer>> indexes = new ArrayList<List<Integer>>();
          List<Integer> temp = new ArrayList<Integer>();
          ans.add(words[0]);
          int index = 0;
          temp.add((ans.get(index)).length());
          for(int i=1;i<words.length;i++){
              if(words[i].length() < (maxWidth-(ans.get(index)).length())){
                  String s = ans.get(index)+" "+words[i];
                  ans.set(index,s);
                  temp.add((ans.get(index)).length());
                  continue;
              }
              indexes.add(temp);
              temp = new ArrayList<Integer>();
              ans.add(words[i]);
              index++;
              temp.add((ans.get(index)).length());
          }
          for(int i=0;i<ans.size()-1;i++){
              if((ans.get(i)).length() < maxWidth){
                  int j=0;
                  while((ans.get(i)).length() != maxWidth){
                      if ((indexes.get(i)).size() == 1){
                          while((ans.get(i)).length() != maxWidth){
                               ans.set(i, ans.get(i)+" ");
                          }
                          break;
                      }
                      if((indexes.get(i)).size()-1 == j){
                          j = 0;
                      }
                      (indexes.get(i)).set(j, (indexes.get(i)).get(j)+j);
                      
                      
                      String s = (ans.get(i)).substring(0, (indexes.get(i)).get(j))+" "+
                                  (ans.get(i)).substring((indexes.get(i)).get(j));
                      ans.set(i, s);
                      j++;
                  }
              }
          }
          while((ans.get(ans.size()-1)).length() != maxWidth){
              ans.set(ans.size()-1, ans.get(ans.size()-1)+" ");
          }
          return ans;
      }
  }
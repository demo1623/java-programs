//For problem statement of this code checkout this link:   (medium level)
//https://leetcode.com/problems/container-with-most-water/   
public class ContainerWithMostWater { 
    public static void main(String[] args) {
        int[] arr = { 1, 8, 6, 2, 5, 4, 8, 3, 7 };
        System.out.println(Helper.maxArea(arr));
    }
}

class Helper {
    public static int maxArea(int[] height) {
        int area = Integer.MIN_VALUE;
        int front = 0;
        int rear = height.length - 1;
        while (front < rear) {
            int mini = Math.min(height[front], height[rear]);
            area = Math.max(area, mini * (rear - front));
            if (height[front] < height[rear]) {
                front++;
            } else {
                rear--;
            }
        }
        return area;
    }
}
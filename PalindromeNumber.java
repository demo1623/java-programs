import java.util.Scanner;
class Helper {
      public static boolean isPalindrome(int x) {
            if (x < 0) {
                  return false;
            }
            int rev = 0;
            int temp = x;
            while (true) {
                  if (temp == 0) {
                        break;
                  }
                  int temp1 = temp % 10;
                  temp /= 10;
                  rev = (rev * 10) + temp1;
            }
            if (rev != x) {
                  return false;
            }
            return true;
      }
}

public class PalindromeNumber {
      public static void main(String args[]){
            Scanner sc = new Scanner(System.in);
            System.out.println("Enter any number: ");
            System.out.println(Helper.isPalindrome(sc.nextInt()));
      }
}

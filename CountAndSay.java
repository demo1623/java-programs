//For problem statement of this code checkout this link:   (medium level)
// https://leetcode.com/problems/count-and-say/  
public class CountAndSay {
      public static void main(String args[]) {
            int n = 5;
            System.out.println("For " + (n) + ": " + Helper.countAndSay(5));
      }
}

class Helper {
      public static String countAndSay(int n) {
            String s = "1";
            String res = "";
            for (int k = 0; k < n - 1; k++) {
                  for (int i = 0; i < s.length(); i++) {
                        if (i == 0) {
                              res += '1';
                              res += s.charAt(i);
                              continue;
                        }
                        if (res.charAt(res.length() - 1) == s.charAt(i)) {
                              char c = (char) ((int) (res.charAt(res.length() - 2)) + 1);
                              res = res.substring(0, res.length() - 2) + c + res.substring(res.length() - 1);
                        } else {
                              res += '1';
                              res += s.charAt(i);
                        }
                  }
                  s = res;
                  res = "";
            }
            return s;
      }

}
class Helper {
      public static int calculate(String s) {
            int num = 0;
            int sum = 0;
            char o = ' ';
            for (int i = 0; i < s.length(); i++) {
                  if ((int) s.charAt(i) >= 48 && (int) s.charAt(i) <= 57) {
                        if (o != ' ' && sum == 0)
                              num = (num * 10) + (s.charAt(i) - '0');
                        else
                              num = (num * 10) + (s.charAt(i) - '0');
                        if (i == s.length() - 1) {
                              if (o == 'p')
                                    sum += num;
                              else if (o == 'm')
                                    sum -= num;
                              else
                                    sum = num;
                        }
                  } else if (!((int) s.charAt(i) >= 48 && (int) s.charAt(i) <= 57) && num != 0) {
                        i--;
                        if (o == 'p')
                              sum += num;
                        else if (o == 'm')
                              sum -= num;
                        else if (o == ' ')
                              sum = num;
                        num = 0;
                        o = ' ';
                  } else if ((int) s.charAt(i) == 43)
                        o = 'p';
                  else if ((int) s.charAt(i) == 45)
                        o = 'm';
                  else if (s.charAt(i) == '(') {
                        if (o == 'm')
                              sum -= calculate(s.substring(i + 1));
                        else
                              sum += calculate(s.substring(i + 1));
                        int balance = 0;
                        while (true) {
                              if (s.charAt(i) == '(')
                                    balance++;
                              else if (s.charAt(i) == ')') {
                                    balance--;
                                    if (balance == 0) {
                                          break;
                                    }
                              }
                              i++;
                        }
                  } else if (s.charAt(i) == ')')
                        break;
            }
            return sum;
      }

}

public class ExpressionCalculator {
      public static void main(String args[]) {
            String expression = "2-(1+(4+3+5)-3)+(6+7)";    //Expression for calculate
            System.out.println(Helper.calculate(expression)); 
      }
}
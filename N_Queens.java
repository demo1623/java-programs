import java.util.*;

public class N_Queens {
      public static void main(String args[]) {
            List<List<String>> ans = Helper.solveNQueens(4);
            for(int i=0;i<ans.size();i++){
                  for(int j=0;j<ans.get(i).size();j++){
                        System.out.println((ans.get(i)).get(j));
                  }
                  System.out.println("_______");
            }
      }
}

class Helper {
      static List<List<String>> ans;

      public static List<List<String>> solveNQueens(int n) {
            ans = new ArrayList<List<String>>();
            List<String> board = new ArrayList<String>();
            String str = "";
            for (int i = 0; i < n; i++) {
                  str += ".";
            }
            for (int i = 0; i < n; i++) {
                  board.add(str);
            }
            // System.out.println(board.get(1));
            Help(board, 0);
            return ans;
      }

      public static void Help(List<String> board, int n) {
            for (int i = 0; i < board.size(); i++) {
                  for (int j = 0; j < board.size(); j++) {
                        if ((board.get(i)).charAt(j) == 'Q') {
                              if (Check(board, i, j)) {
                                    return;
                              }
                        }
                  }
            }
            if (board.size() == n) {
                  List<String> clone = new ArrayList<String>(board);
                  ans.add(clone);
                  return;
            }

            for (int i = 0; i < board.size(); i++) {
                  board.set(n, (board.get(n)).substring(0, i) + 'Q' + (board.get(n)).substring(i + 1));
                  Help(board, n + 1);
                  board.set(n, (board.get(n)).substring(0, i) + '.' + (board.get(n)).substring(i + 1));
            }
      }

      public static boolean Check(List<String> board, int i, int j) {
            int[] U = { i }, D = { i }, L = { j }, R = { j }, BR = { i, j }, BL = { i, j }, TR = { i, j },
                        TL = { i, j };
            int n = board.size() - 1;
            while (true) {
                  boolean bool = false;
                  if (U[0] != 0) {
                        U[0] -= 1;
                        if ((board.get(U[0])).charAt(j) == 'Q') {
                              return true;
                        }
                        bool = true;
                  }
                  if (D[0] != n) {
                        D[0] += 1;
                        if ((board.get(D[0])).charAt(j) == 'Q') {
                              return true;
                        }
                        bool = true;
                  }
                  if (L[0] != 0) {
                        L[0] -= 1;
                        if ((board.get(i)).charAt(L[0]) == 'Q') {
                              return true;
                        }
                        bool = true;
                  }
                  if (R[0] != n) {
                        R[0] += 1;
                        if ((board.get(i)).charAt(R[0]) == 'Q') {
                              return true;
                        }
                        bool = true;
                  }
                  if (BR[0] != n && BR[1] != n) {
                        BR[0] += 1;
                        BR[1] += 1;
                        if ((board.get(BR[0])).charAt(BR[1]) == 'Q') {
                              return true;
                        }
                        bool = true;
                  }
                  if (BL[0] != n && BL[1] != 0) {
                        BL[0] += 1;
                        BL[1] -= 1;
                        if ((board.get(BL[0])).charAt(BL[1]) == 'Q') {
                              return true;
                        }
                        bool = true;
                  }
                  if (TR[0] != 0 && TR[1] != n) {
                        TR[0] -= 1;
                        TR[1] += 1;
                        if ((board.get(TR[0])).charAt(TR[1]) == 'Q') {
                              return true;
                        }
                        bool = true;
                  }
                  if (TL[0] != 0 && TL[1] != 0) {
                        TL[0] -= 1;
                        TL[1] -= 1;
                        if ((board.get(TL[0])).charAt(TL[1]) == 'Q') {
                              return true;
                        }
                        bool = true;
                  }
                  if (bool == false) {
                        break;
                  }
            }
            return false;
      }
}
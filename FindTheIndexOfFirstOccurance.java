//This program will find the first occurance of needle String in haystack String 
public class FindTheIndexOfFirstOccurance {
      public static void main(String args[]) {
            System.out.println("Index: " + Helper.Find("HappyButHappy", "But"));
      }
}

class Helper {
      public static int Find(String haystack, String needle) {
            if (haystack.length() < needle.length()) {
                  return -1;
            }
            for (int i = 0; i <= haystack.length() - needle.length(); i++) {
                  String s = "";
                  for (int j = i; j < i + needle.length(); j++) {
                        s += haystack.charAt(j);
                  }
                  if (needle.equals(s)) {
                        return i;
                  }
            }
            return -1;
      }
}